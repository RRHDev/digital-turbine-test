﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using DTTest.Infrastructure;
using DTTest.Models;

namespace DTTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //List of Races
            var raceValues = from CustomEnum.Race e in Enum.GetValues(typeof(CustomEnum.Race))
                             select new { Id = (int)e, Name = e.ToString() };

            ViewBag.RaceList = raceValues;

            // List of people
            var peopleList = getPeopleList().ToList();

            if (Request.QueryString["race"] != null)
            {
                var n = (from p in peopleList where ((int)p.Race).ToString().Equals(Request.QueryString["race"]) && (p.Age % 2).Equals(0) orderby p.Age select p).ToList();

                peopleList = n;
            }


            ViewBag.People = peopleList;

            return View();
        }

        // GET api/
        public ActionResult JsonDataResult()
        {
            var peopleList = getPeopleList().ToList();

            var Total = peopleList.Count;

            var aveAge = peopleList.Average(p => p.Age);
            var minAge = peopleList.Min(p => p.Age);
            var maxAge = peopleList.Max(p => p.Age);
            var Ages = new { Average = aveAge, Min = minAge, Max = maxAge };

            var Angles = (from p in peopleList where p.Race.Equals(CustomEnum.Race.Angles) select p).Count();
            var Saxons = (from p in peopleList where p.Race.Equals(CustomEnum.Race.Saxons) select p).Count();
            var Jutes = (from p in peopleList where p.Race.Equals(CustomEnum.Race.Jutes) select p).Count();
            var Asians = (from p in peopleList where p.Race.Equals(CustomEnum.Race.Asians) select p).Count();

            return this.Json(
                  new { Total, Ages, Races = new { Angles, Saxons, Jutes, Asians } }, JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<Person> getPeopleList()
        {
            IEnumerable<Person> peopleList;

            if (HttpRuntime.Cache["people"] != null)
            {
                // Populicate people list from cache if exists
                peopleList = (List<Person>)HttpRuntime.Cache["people"];
            }
            else
            {
                // Re-generete people list and push into cache
                peopleList = Utility.IncreaseAge(Person.InitPeople(), 1);
                HttpRuntime.Cache.Insert("people", peopleList, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(10), CacheItemPriority.High, null);
            }

            return peopleList;
        }
    }
}
