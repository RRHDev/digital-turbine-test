﻿using System;
using System.Collections.Generic;
using DTTest.Infrastructure;

namespace DTTest.Models
{
    public class Person
    {
        public Person()
        {

        }

        public Person(string name, int age)
        {
            this.Name = name;
            this.Age = age;
        }

        public string Name { get; set; }

        public int Age { get; set; }

        public CustomEnum.Race Race { get; set; }

        public Double Height
        {
            get { return this.GetHeight(this.Race); }
            set { throw new NotImplementedException(); }
        }

        private double GetHeight(CustomEnum.Race race)
        {
            double h = 0.0;
            switch (race)
            {
                case CustomEnum.Race.Angles:
                case CustomEnum.Race.Saxons:
                    h = (1.5 + Age * 0.45); break;
                case CustomEnum.Race.Jutes:
                    h = this.Age * 1.6 / 2;
                    break;
                case CustomEnum.Race.Asians:
                    h = 1.7 + ((this.Age + 2) * 0.23);
                    break;
            }

            return h;
        }

        public override string ToString()
        {

            return "My name is '" + Name + "' and I am " + Age + " years old.";

        }

        public static List<Person> InitPeople()
        {

            var people = new List<Person>();

            var rnd = new Random();

            for (int i = 0; i < 10000; i++)
            {

                people.Add(new Person()
                {

                    Name = "Person #" + i,

                    Age = rnd.Next(1, 99),

                    Race = (CustomEnum.Race)rnd.Next(1, 5)

                });

            }

            return people;

        }

        public List<Person> IncreaseAge(List<Person> people, int year)
        {

            if (people != null)
            {
                foreach (var p in people)
                {
                    p.Age += 1;
                }
            }

            return people;


        }
    }
}