﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTTest.Models;

namespace DTTest.Infrastructure
{
    public class Utility
    {
        public Utility()
        {

        }

        public static SelectList ToSelectList<TEnum>(TEnum enumObj)
        {

            var values = (from TEnum e in Enum.GetValues(typeof(TEnum))

                          select new { Id = e, Name = e.ToString() }).ToList();



            return new SelectList(values, "Id", "Name", enumObj);

        }

        

        public static List<Person> IncreaseAge(List<Person> people, int year)
        {

            if (people != null)
            {
                foreach (var p in people)
                {
                    p.Age += 1;
                }
            }

            return people;


        }
    }
}